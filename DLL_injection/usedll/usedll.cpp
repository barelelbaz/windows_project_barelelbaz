#define LIBRARY "C:\\Users\\magshimim\\Desktop\\dll\\DLL_injection\\x64\\Release\\DLL_injection.dll"
#include <Windows.h>
#include <tlhelp32.h>
#include <stdio.h>
DWORD FindProcessId(const char *processname)
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	DWORD result = NULL;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hProcessSnap) return(FALSE);

	pe32.dwSize = sizeof(PROCESSENTRY32); // <----- IMPORTANT

										  // Retrieve information about the first process,
										  // and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
	{
		CloseHandle(hProcessSnap);          // clean the snapshot object
		printf("!!! Failed to gather information on system processes! \n");
		return(NULL);
	}

	do
	{
		if (0 == strcmp(processname, pe32.szExeFile))
		{
			result = pe32.th32ProcessID;
			break;
		}

	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);

	return result;
}
int main()
{
	const char* path = "C:\\Users\\magshimim\\Desktop\\dll\\DLL_injection\\x64\\Release\\DLL_injection.dll";
	DWORD err;
	BOOL check;
	int procID = 0;
	while (!procID)
	{
		procID = FindProcessId("notepad.exe");
	}
	// Get LoadLibrary function address �
	// the address doesn't change at remote process
		// Open remote process
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS ,FALSE, procID);
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");

	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
	PVOID memAddr = (PVOID)VirtualAllocEx(proc, NULL, strlen(path), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (NULL == memAddr) {
		err = GetLastError();
		return 0;
	}
	// Write DLL name to remote process memory
	check = WriteProcessMemory(proc, memAddr, path, strlen(path), NULL);
	if (0 == check) {
		err = GetLastError();
		return 0;
	}
	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	HANDLE hRemote = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE) addrLoadLibrary, memAddr, NULL, NULL);
	if (NULL == hRemote) {
		err = GetLastError();
		return 0;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	return 0;
}